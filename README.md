ProtoApp | ID Recognition
=========================

Not much here yet. Basic idea: show camera view and outline of ID card in the image (using tooling from [id-recognition](https://gitlab.com/ripadp/id-recognition)), and when tapped OCR the different fields.

Tools we'll use:
- [tesseract_ocr](https://pub.dev/packages/tesseract_ocr) plugin
- [camera](https://pub.dev/packages/camera) plugin
- [Platform channels](https://flutter.dev/docs/development/platform-integration/platform-channels) for [OpenCV on Android](https://opencv.org/android/)
- [TensorFlow Lite Inference](https://www.tensorflow.org/lite/guide/inference) and [tflite](https://pub.dev/packages/tflite) plugin for segmentation

UI ideas:
- provide a frame and guides like [ReadID](https://readid.com/) to position the card in front of the camera
  - NFC authentication here follows [Basic access control](https://en.wikipedia.org/wiki/Basic_access_control)
  - the actual NFC reading is also implemented open source in [tananaev/passport-reader](https://github.com/tananaev/passport-reader)
- show the `<<<<<<` lines ("MRZ lines") for the front side
- turn everything green when the card is found, and print the recognised fields in green too

We can also verify some of the fields against the [MRZ lines](https://fr.wikipedia.org/wiki/Carte_nationale_d%27identit%C3%A9_en_France#Codage_bande_MRZ_(lecture_optique)).
